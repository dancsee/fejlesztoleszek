<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST')
{
  header('HTTP/1.1 403 Forbidden');
  echo "<h1>403, azaz négyszázhárom. Ne turkálj.</h1>";
  die();
}

$source = 'content.json';

if (is_file('content.json'))
{
  $file_content = file_get_contents('content.json');

  if (!empty($file_content))
  {
    $content = json_decode($file_content);

    if (isset($content->content) && !empty($content->content))
    {
      header('Content-Type: application/json');
      header('HTTP/1.1 200 OK');
      print json_encode(array("content" => $content->content));
    }
    else
    {
      header('Content-Type: application/json');
      header('HTTP/1.1 500 Internal Server Error');
      print json_encode(array("message" => "Hibás vagy hiányzó fájltartalom."));
    }
  }
  else
  {
    header('Content-Type: application/json');
    header('HTTP/1.1 500 Internal Server Error');
    print json_encode(array("message" => "Üres bemeneti fájl."));
  }
}
else
{
  header('Content-Type: application/json');
  header('HTTP/1.1 500 Internal Server Error');
  print json_encode(array("message" => "Bemeneti fájl nem található."));
}

?>
