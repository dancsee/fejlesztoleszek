# Ez az önéletrajzom publikus repoja

Igazából teljesen fölösleges a readme, csak azért írom, hogy bizonyítsam, írtam már markdown fájlt.

### Ha kell tudok
* Felsorolást írni

```javascript
function kódrészletet()
{
  return írni;
}
```

| Meg ha nagyon muszáj                              |   Táblázatot is |
| :-----                                  | :----:     | :----:                              | :-----:                           |
| Fölösleges sor 1. | **félkövér cucc**                             |
| Fölösleges sor 2. |                             *dőlt cucc*                             |

Na szóval ilyenek.. amúgy meg lehet turkálni a kódot.
